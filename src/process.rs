#[cfg(windows)]
use winapi;
type ProcessHandle = winapi::um::winnt::HANDLE;
type ProcessEntry = winapi::um::tlhelp32::PROCESSENTRY32;
use std::convert::TryInto;
use std::ffi::CStr;
use std::mem::size_of;

use async_std::task;

mod errors;

//

pub struct Process {
    pub name: String,
    handle: usize,
}

impl Process {
    pub async fn open(name: String) -> Result<Self, errors::ProcessErrors> {
        let exe_name = name.clone();
        let handle_task = task::spawn(async move {
            let mut entry: ProcessEntry = unsafe { std::mem::MaybeUninit::zeroed().assume_init() };
            entry.dwSize = size_of::<winapi::um::tlhelp32::PROCESSENTRY32>()
                .try_into()
                .unwrap();
            let mut h_process: Option<ProcessHandle> = None;
            let snapshot = unsafe {
                winapi::um::tlhelp32::CreateToolhelp32Snapshot(
                    winapi::um::tlhelp32::TH32CS_SNAPPROCESS,
                    0,
                )
            };
            if unsafe { winapi::um::tlhelp32::Process32First(snapshot, &mut entry) } > 0 {
                while unsafe { winapi::um::tlhelp32::Process32Next(snapshot, &mut entry) } > 0 {
                    if unsafe { CStr::from_ptr(std::mem::transmute(&entry.szExeFile)) }
                        .to_string_lossy()
                        .into_owned()
                        == exe_name
                    {
                        let proc_handle = unsafe {
                            winapi::um::processthreadsapi::OpenProcess(
                                winapi::um::winnt::PROCESS_ALL_ACCESS,
                                0,
                                entry.th32ProcessID,
                            )
                        };

                        if proc_handle.is_null() {
                            return Err(errors::ProcessErrors::OpenFailed);
                        }
                        h_process = Some(proc_handle);
                        break;
                    }
                }
            }

            unsafe { winapi::um::handleapi::CloseHandle(snapshot) };

            if let Some(h_process) = h_process {
                return Ok(unsafe { std::mem::transmute::<ProcessHandle, usize>(h_process) });
            }

            return Err(errors::ProcessErrors::OpenFailed);
        });

        let p_handle = handle_task.await?;

        Ok(Self {
            name,
            handle: p_handle,
        })
    }

    pub async fn read_process_memory(
        &self,
        adress: usize,
        count: usize,
    ) -> Result<Vec<u8>, errors::ProcessErrors> {
        let handle = self.handle.clone();
        let read_task = task::spawn(async move {
            let mut buff = Vec::<u8>::with_capacity(count);
            let mut bytes_read = 0usize;
            unsafe {
                if winapi::um::memoryapi::ReadProcessMemory(
                    std::mem::transmute(handle),
                    std::mem::transmute(adress),
                    std::mem::transmute(buff.as_mut_ptr()),
                    count,
                    &mut bytes_read,
                ) == 0
                {
                    return Err(errors::ProcessErrors::ReadFailed);
                }
            }
            Ok(buff)
        });

        read_task.await
    }
}
