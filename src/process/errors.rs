use thiserror::Error;

#[derive(Error, Debug)]
pub enum ProcessErrors
{
    #[error("Failed to open process handle")]
    OpenFailed,
    #[error("Failed to read process memory")]
    ReadFailed,
    
}